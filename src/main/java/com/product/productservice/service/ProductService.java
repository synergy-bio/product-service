package com.product.productservice.service;

import com.product.productservice.model.Product;
import com.product.productservice.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public List<Product> getProductByName(String productName) {
        if (Objects.nonNull(productName)) {
            return productRepository.findByProductNameLike(productName);
        }
        return productRepository.findAll();
    }

    public Optional<Product> getProductById(Integer id) {
        return productRepository.findById(id);
    }

    public Product createOrUpdateProduct(Product product) {
        return productRepository.saveAndFlush(product);
    }
}
