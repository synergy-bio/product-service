package com.product.productservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum RawMaterialType {

    ELECTRICAL("electrical", 0),
    HARDWARE("hardware", 1),
    ELECTRONIC("electronic", 2),
    MISC("misc", 3);

    @Getter
    private final String type;

    @Getter
    private final Integer typeId;
}
