package com.product.productservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data @AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class ProductRawMaterialKey implements Serializable {

    @Column(name = "product_id")
    Integer productId;

    @Column(name = "raw_material_id")
    Integer rawMaterialId;

}
