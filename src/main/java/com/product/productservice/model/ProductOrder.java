package com.product.productservice.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "product_order")
public class ProductOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private ProductOrderStatusType productOrderStatus;

    @Column(name = "selling_price")
    private Double sellingPrice;

    private Double cost;

    private Double discount;

    @Column(name = "missing_raw_item_flag")
    private Boolean missingRawItemFlag;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

}
