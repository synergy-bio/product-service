package com.product.productservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum MeasureUnit {

    KILOGRAM("kilogram", 0),
    METER("meter", 1),
    LITRE("litre", 2);

    @Getter
    private final String unit;

    @Getter
    private final Integer unitId;
}
