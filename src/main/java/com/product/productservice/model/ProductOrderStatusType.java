package com.product.productservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum ProductOrderStatusType {

    DRAFT("open", 0),
    SUBMITTED("completed", 1);

    @Getter
    private final String productOrderStatus;

    @Getter
    private final Integer statusId;
}
