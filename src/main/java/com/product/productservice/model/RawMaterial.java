package com.product.productservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@EqualsAndHashCode(exclude = "productRawMaterials")
@Table(name = "raw_material")
public class RawMaterial {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String rawMaterialName;

    @Column(name = "limit")
    private Double limit;

    @Column(name = "barcode")
    private String barcode;

    @Column(name = "measure_unit")
    private MeasureUnit measureUnit;

    @Column(name = "type")
    private RawMaterialType type;

    @Column(name = "is_serial_available")
    private Boolean isSerialAvailable;

    //@JsonBackReference
    //@JsonIgnore
    @JsonIgnoreProperties("rawMaterial")
    @OneToMany(mappedBy = "rawMaterial", cascade = CascadeType.ALL)
    List<ProductRawMaterial> productRawMaterials = new ArrayList<>();

}
