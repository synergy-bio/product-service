package com.product.productservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "product_raw_material")
public class ProductRawMaterial {

    @EmbeddedId
    ProductRawMaterialKey id;


    @ManyToOne
    @MapsId("rawMaterialId")
    @JoinColumn(name = "raw_material_id")
    @JsonIgnoreProperties("productRawMaterials")
    RawMaterial rawMaterial;


    @ManyToOne
    @MapsId("productId")
    @JoinColumn(name = "product_id")
    @JsonIgnoreProperties("productRawMaterials")
    Product product;

    @Column(name = "quantity")
    Double quantity;


    public ProductRawMaterial(ProductRawMaterialKey id, RawMaterial rawMaterial, Product product, Double quantity) {
        this.id = new ProductRawMaterialKey(product.getId(), rawMaterial.getId());
        this.rawMaterial = rawMaterial;
        this.product = product;
        this.quantity = quantity;
    }
}
