package com.product.productservice.controller;

import com.product.productservice.model.Product;
import com.product.productservice.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/product", name = "Product Management")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PreAuthorize(value = "hasRole('ROLE_admin') or hasRole('ROLE_operator')")
    @GetMapping(value = "/all", name = "Get All Product")
    public ResponseEntity<List<Product>> getAllProducts() {
        return ResponseEntity.ok(productService.getAllProducts());
    }

    @PreAuthorize(value = "hasRole('ROLE_admin') or hasRole('ROLE_operator')")
    @GetMapping(value = "/{product-name}", name = "Get Product By Name")
    public ResponseEntity<List<Product>> getProductByName(@PathVariable("product-name") String productName) {
        return ResponseEntity.ok(productService.getProductByName(productName));
    }

    @PreAuthorize(value = "hasRole('ROLE_admin') or hasRole('ROLE_operator')")
    @GetMapping(value = "/{product-id}", name = "Get Product By Id")
    public ResponseEntity<Product> getProductByName(@PathVariable("product-id") Integer id) {
        return productService.getProductById(id)
                .map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
    }

    @PreAuthorize(value = "hasRole('ROLE_admin') or hasRole('ROLE_operator')")
    @PostMapping(value = "/create", name = "Create New Product")
    public ResponseEntity<Product> createNewProduct(@RequestBody Product product) {
        return ResponseEntity.ok(productService.createOrUpdateProduct(product));
    }

    @PreAuthorize(value = "hasRole('ROLE_admin') or hasRole('ROLE_operator')")
    @PutMapping(value = "/update", name = "Update New Product")
    public ResponseEntity<Product> updateNewProduct(@RequestBody Product product) {
        return ResponseEntity.ok(productService.createOrUpdateProduct(product));
    }
}
